﻿using System;
using System.Drawing;

namespace P4M03Uf4OlavMartos
{
    // Clase Cercle
    public class Cercle : FiguraGeometrica, IOrdenable
    {
        // Atributo
        private double radi;
        public double GetRadi() { return radi; }
        public void SetRadi(double value) { radi = value; }

        // Tres constructores: uno vacio, otro al que se le pasan datos y otro que es una copia de otra instancia
        public Cercle() { }
        public Cercle(int codi, string nom, Color color, double radi) : base(codi, nom, color) { this.radi = radi; }
        public Cercle(Cercle Cer)
        {
            SetCodi(Cer.GetCodi());
            SetNom(Cer.GetNom());
            SetColor(Cer.GetColor());
            SetRadi(Cer.GetRadi());
        }

        // Metodo abstracto
        public override double CalcularArea() { return Math.Round(Math.PI * Math.Pow(radi, 2), 2); }

        // Metodos
        private double Perimetre() { return Math.Round(2 * Math.PI * radi, 2); }

        public override string ToString() { return $"{base.ToString()}, Radi={radi}, Perimetre={Perimetre()}, Area={CalcularArea()}"; }

        // Metodos sobreescritos
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType()) { return false; }

            Cercle other = (Cercle)obj;
            return GetCodi() == other.GetCodi();
        }

        public override int GetHashCode() { return GetCodi().GetHashCode(); }

        // Metodo Comparar de la interficie
        public int Comparar(IOrdenable otraFigura)
        {
            Cercle cercle = (Cercle)otraFigura;

            if (CalcularArea() == cercle.CalcularArea()) { return 0; }
            else if (CalcularArea() < cercle.CalcularArea()) { return -1; }
            else { return 1; }
        }
    }
}
