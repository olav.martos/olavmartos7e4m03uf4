﻿using System.Drawing;

namespace P4M03Uf4OlavMartos
{
    // Clase FiguraGeometrica
    public abstract class FiguraGeometrica
    {
        // Atributos con sus Getters y Setters
        protected int codi;

        public int GetCodi() { return codi; }
        public void SetCodi(int value) { codi = value; }

        protected string nom;

        public string GetNom() { return nom; }

        public void SetNom(string value) { nom = value; }

        protected Color color;
        public Color GetColor() { return color; }
        public void SetColor(Color value) { color = value; }

        // Tres constructores: uno vacio, otro al que se le pasan datos y otro que es una copia de otra instancia
        public FiguraGeometrica() { }
        public FiguraGeometrica(int codi, string nom, Color color)
        {
            this.codi = codi;
            SetNom(nom);
            this.color = color;
        }
        public FiguraGeometrica(FiguraGeometrica figura)
        {
            SetCodi(figura.GetCodi());
            SetNom(figura.GetNom());
            SetColor(figura.GetColor());
        }

        // Metodo abstracto
        public abstract double CalcularArea();

        // Metodo
        public override string ToString() { return $"Codi={codi}, Nom={GetNom()}, {color}"; }

        // Metodos sobreescritos
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType()) { return false; }
            FiguraGeometrica figura = (FiguraGeometrica)obj;
            return codi == figura.codi;
        }

        public override int GetHashCode() { return codi.GetHashCode(); }

    }
}
