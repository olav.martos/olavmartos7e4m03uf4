﻿namespace P4M03Uf4OlavMartos
{
    /// <summary>
    /// Anteriormente, en el documento se decia que el metodo Ordenar tenia que estar en esta clase, pero que ahora
    /// tiene que estar en una clase llamada Data, que ya existe y tiene su metodo Comparar, debido a esto se ha dejado esta clase, puesto
    /// que todo ya se habia creado cuando aun estaba escrita esta clase en el documento.
    /// </summary>
    public class Algorismes
    {
        /// <summary>
        /// Funcion que ordena una tabla de objetos gracias al metodo Comparar que poseen cada clase de objetos que contienen dicho metodo
        /// que pertenece a la Interficie IOrdenable.
        /// </summary>
        /// <param name="obj">Tabla de objetos pasada como parametros. Puede ser de 4 clases diferentes: Rectangle, Triangle, Cercle y Data</param>
        public static void Ordenar(IOrdenable[] obj)
        {
            for (int i = 0; i < obj.Length - 1; i++)
            {
                for (int j = 0; j < obj.Length - i - 1; j++)
                {
                    // El metodo Comparar puede devolver diversos numeros, entre ellos el 1. Si devuelve este numero se ordenara los dos elementos que estaba comparando
                    if (obj[j].Comparar(obj[j + 1]) > 0)
                    {
                        IOrdenable temp = obj[j];
                        obj[j] = obj[j + 1];
                        obj[j + 1] = temp;
                    }
                }
            }
        }
    }
}
