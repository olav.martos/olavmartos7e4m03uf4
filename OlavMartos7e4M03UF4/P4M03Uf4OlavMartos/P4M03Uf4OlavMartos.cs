﻿using System;
using System.Drawing;

namespace P4M03Uf4OlavMartos
{
    class ProvaOrdenar
    {
        static void Main(string[] args)
        {
            //Rectangles();
            //Triangles();
            //Cercles();
            Datas();
            Console.ResetColor();

        }

        /// <summary>
        /// Crea una tabla de Rectangulos, los muestra por pantalla, los ordena y los vuelve a mostrar ya ordenados
        /// </summary>
        static void Rectangles()
        {
            Console.ForegroundColor = ConsoleColor.Blue;

            // Creamos una tabla de Rectangulos
            IOrdenable[] figuras = new IOrdenable[]
            {
                new Rectangle(1, "Rect1", Color.Red, 10, 6),
                new Rectangle(2, "Rect2", Color.Blue, 5, 10),
                new Rectangle(3, "Rect3", Color.Green, 7, 7)
            };

            // Creamos una tabla de Rectangulos
            Console.WriteLine("Lista original de Rectangle: ");
            foreach (IOrdenable figura in figuras) { Console.WriteLine(figura); }

            // Ordenamos la tabla
            Algorismes.Ordenar(figuras);

            // Mostramos la tabla ya ordenada
            Console.WriteLine("\nLista ordenada de Rectangle:");
            foreach (IOrdenable figura in figuras) { Console.WriteLine(figura); }
        }

        /// <summary>
        /// Crea una tabla de Triangulos, los muestra por pantalla, los ordena y los vuelve a mostrar ya ordenados
        /// </summary>
        static void Triangles()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;

            // Creamos una tabla de Triangulos
            IOrdenable[] figuras = new IOrdenable[]
            {
                new Triangle(1, "Tri1", Color.Red, 10, 6),
                new Triangle(2, "Tri2", Color.Blue, 5, 12),
                new Triangle(3, "Tri3", Color.Green, 7, 7)
            };

            // Mostramos la tabla original
            Console.WriteLine("Lista original de Triangle: ");
            foreach (IOrdenable figura in figuras) { Console.WriteLine(figura); }

            // Ordenamos la tabla
            Algorismes.Ordenar(figuras);

            // Mostramos la tabla ya ordenada
            Console.WriteLine("\nLista ordenada de Triangle:");
            foreach (IOrdenable figura in figuras) { Console.WriteLine(figura); }
        }

        /// <summary>
        /// Crea una tabla de Circulos, los muestra por pantalla, los ordena y los vuelve a mostrar ya ordenados
        /// </summary>
        static void Cercles()
        {
            Console.ForegroundColor = ConsoleColor.Red;

            // Creamos una tabla de Circulos
            IOrdenable[] figuras = new IOrdenable[]
            {
                new Cercle(1, "Cer1", Color.Red, 10),
                new Cercle(2, "Cer2", Color.Blue, 5),
                new Cercle(3, "Cer3", Color.Green, 7)
            };

            // Mostramos la tabla original
            Console.WriteLine("Lista original de Cercle: ");
            foreach (IOrdenable figura in figuras) { Console.WriteLine(figura); }

            // Ordenamos la tabla
            Algorismes.Ordenar(figuras);

            // Mostramos la tabla ya ordenada
            Console.WriteLine("\nLista ordenada de Cercle:");
            foreach (IOrdenable figura in figuras) { Console.WriteLine(figura); }
        }

        /// <summary>
        /// Crea una tabla de Datas, las muestra por pantalla, las ordena y las vuelve a mostrar ya ordenadas
        /// </summary>
        static void Datas()
        {
            Console.ForegroundColor = ConsoleColor.Green;

            // Creamos una tabla de Datas
            Data[] tabla = new Data[]
            {
                new Data(5, 3, 2023),
                new Data(20, 2, 2023),
                new Data(15, 7, 2023),
                new Data(1, 1, 2022),
                new Data(31, 12, 2022)
            };

            // Mostramos la tabla original
            Console.WriteLine("Tabla de Data, antes de ordenarla:");
            foreach (Data d in tabla) { Console.WriteLine(d); }
            Console.WriteLine();

            // Ordenamos la tabla
            Algorismes.Ordenar(tabla);

            // Mostramos la tabla ya ordenada
            Console.WriteLine("Tabla de Data, después de ordenarla:");
            foreach (Data d in tabla) { Console.WriteLine(d); }
        }
    }
}
