﻿using System.Drawing;

namespace P4M03Uf4OlavMartos
{
    // Clase Rectangle
    public class Rectangle : FiguraGeometrica, IOrdenable
    {
        // Atributos
        private double rectBase;
        public double GetBase() { return rectBase; }
        public void SetBase(double value) { rectBase = value; }


        private double altura;
        public double GetAltura() { return altura; }
        public void SetAltura(double value)
        { altura = value; }

        // Tres constructores: uno vacio, otro al que se le pasan datos y otro que es una copia de otra instancia
        public Rectangle() { }
        public Rectangle(int codi, string nom, Color color, double baseRect, double altura) : base(codi, nom, color)
        {
            rectBase = baseRect;
            this.altura = altura;
        }
        public Rectangle(Rectangle rect)
        {
            SetCodi(rect.GetCodi());
            SetNom(rect.GetNom());
            SetColor(rect.GetColor());
            SetBase(rect.GetBase());
            SetAltura(rect.GetAltura());
        }

        // Metodo abstracto
        public override double CalcularArea() { return rectBase * altura; }

        // Metodos
        private double Perimetre() { return 2 * (rectBase + altura); }
        public override string ToString() { return $"{base.ToString()}, Base={rectBase}, Altura={altura}, Perimetre={Perimetre()}, Area={CalcularArea()}"; }

        // Metodos sobreescritos
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType()) { return false; }

            Rectangle other = (Rectangle)obj;
            return GetCodi() == other.GetCodi();
        }

        public override int GetHashCode() { return GetCodi().GetHashCode(); }

        // Metodo Comparar de la interficie
        public int Comparar(IOrdenable otraFigura)
        {
            Rectangle rectangulo = (Rectangle)otraFigura;

            if (CalcularArea() == rectangulo.CalcularArea()) { return 0; }
            else if (CalcularArea() < rectangulo.CalcularArea()) { return -1; }
            else { return 1; }
        }
    }
}
