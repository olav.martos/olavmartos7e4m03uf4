﻿using System;
using System.Globalization;

namespace P3M03Uf4OlavMartos
{
    public class Data
    {
        // Atributos con sus Getters y setters
        public int day;

        public int GetDay() { return day; }

        public void SetDay(int day)
        {
            if (EsCorrecta(day, month, year)) { this.day = day; }
        }

        public int month;

        public int GetMonth() { return month; }

        public void SetMonth(int month)
        {
            if (EsCorrecta(day, month, year)) { this.month = month; }
        }

        public int year;

        public int GetYear() { return year; }

        public void SetYear(int year)
        {
            if (EsCorrecta(day, month, year)) { this.year = year; }
        }

        // Constructores

        /// <summary>
        /// Constructor normal
        /// </summary>
        /// <param name="d">Dia pasado</param>
        /// <param name="m">Mes pasado</param>
        /// <param name="y">Año pasado</param>
        public Data(int d, int m, int y)
        {
            // Comprobamos si no es correcta, si no lo es, cambios los datos de las variables para obtener la fecha 01-01-1980
            if (!EsCorrecta(d, m, y))
            {
                d = 1;
                m = 1;
                y = 1980;
            }

            // Guardamos el contenido de las variables en sus respectivos atributos
            day = d;
            month = m;
            year = y;
        }

        /// <summary>
        /// Constructor que crea una instancia de Data copiando a otra instancia de Data
        /// </summary>
        /// <param name="dataACopiar">Instancia de Data pasada por parametro</param>
        public Data(Data dataACopiar)
        {
            day = dataACopiar.day;
            month = dataACopiar.month;
            year = dataACopiar.year;
        }

        /// <summary>
        /// Constructor vacio
        /// </summary>
        public Data()
        {
            day = 1;
            month = 1;
            year = 1980;
        }

        // Metodos

        /// <summary>
        /// Comprobamos que la fecha es correcta
        /// </summary>
        /// <param name="dia">Dia pasado</param>
        /// <param name="mes">Mes pasado</param>
        /// <param name="any">Año pasado</param>
        /// <returns>Devuelve true si la fecha es correcta. False si no lo es</returns>
        private bool EsCorrecta(int dia, int mes, int any)
        {
            bool esCorrecta = true;
            int maxDies = 0;

            // Le ponemos un limite dias segun el mes pasado como parametro
            switch (mes)
            {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    maxDies = 31;
                    break;
                case 4:
                case 6:
                case 9:
                case 11:
                    maxDies = 30;
                    break;
                case 2:
                    if ((any % 4 == 0 && any % 100 != 0) || any % 400 == 0)
                    {
                        maxDies = 29;
                    }
                    else
                    {
                        maxDies = 28;
                    }
                    break;
                default:
                    esCorrecta = false;
                    break;
            }

            // Si el dia pasado como parametro es menor a 1 o mayor que el limite se dira que es una fecha incorrecta

            if (dia < 1 || dia > maxDies) { esCorrecta = false; }
            return esCorrecta;
        }

        /// <summary>
        /// Metodo que suma un numero tanto positivo como negativo de dias a una fecha especifica.
        /// <para>La fecha en la que se usa el metodo ha de mantener su contenido</para>
        /// </summary>
        /// <param name="dias">Cantidad de dias a aumentar o disminuir</param>
        /// <returns></returns>
        public Data Sumar(int dias)
        {
            // Creamos una instancia DateTime de la fecha que usamos para asi poder añadir mas facilmente los dias usando el metodo de DateTime, AddDays(x)
            DateTime newDate = new DateTime(year, month, day);
            newDate = newDate.AddDays(dias);

            // Creamos una Data que contendra los nuevos valores que el usuario a introducido
            Data suma = new Data();
            suma.day = newDate.Day;
            suma.month = newDate.Month;
            suma.year = newDate.Year;

            // Devolvemos esta Data y el usuario o la muestra directamente por pantalla o la guarda en una variable para usarla luego
            return suma;
        }

        /// <summary>
        /// Calculamos la diferencia en dias que separan a dos fechas. La fecha a la que le llamamos el metodo y otra que pasamos por
        /// parametro
        /// </summary>
        /// <param name="other">Data pasado por parametro</param>
        /// <returns></returns>
        public int Separacion(Data other)
        {
            // Convertir les dates a ticks
            long ticksDataActual = new DateTime(year, month, day).Ticks;
            long ticksAltraData = new DateTime(other.year, other.month, other.day).Ticks;

            // Restar els ticks i convertir el resultat a dies
            int diferenciaEnDies = (int)((ticksDataActual - ticksAltraData) / TimeSpan.TicksPerDay);

            return diferenciaEnDies;
        }

        /// <summary>
        /// Comparamos dos fechas. La que llama al metodo y otra pasada como parametro
        /// </summary>
        /// <param name="other">Data pasada como parametro</param>
        /// <returns></returns>
        public string Comparar(Data other)
        {
            // Las convertimos en instancias de DateTime para poder usar su metodo CompareTo.
            DateTime d1 = new DateTime(year, month, day);
            DateTime d2 = new DateTime(other.year, other.month, other.day);


            /* El metodo CompareTo devuelve tres posibles resultados:
                 * Si son iguales, devuelve 0
                 * Si la primera fecha es posterior a la segunda, devuelve 1
                 * Si la primera fecha es anterior a la segunda, devuelve -1
             * Se ejecuta un switch para retornar un mensaje para que el usuario sepa que pasa con cada fecha.
             */
            switch (d1.CompareTo(d2))
            {
                case 0:
                    return $"{this} y {other}: Son iguales";
                case 1:
                    return $"{this} es posterior a {other}";
                case -1:
                    return $"{this} es anterior a {other}";
            }

            // Debido a que todos los caminos tienen que retornar algo le pasamos este mensaje que si se ha mostrado por pantalla, es que algo ha pasado
            return "Si te he salido por pantalla, corre! Yo no tendria que haber salido a no ser que... \n¿has comentado/borrado el switch del ComparteTo, verdad?";
        }

        // Sobreescribimos el metodo ToString para que la fecha tenga este formato dd-mm-yyyy
        public override string ToString()
        {
            return $"{day}-{month}-{year}";
        }
    }
    class P3M03Uf4OlavMartos
    {
        static void Main(string[] args)
        {
            // Construimos tres fechas segun los constructores
            Data data = new Data(10, 12, 2020);
            Data data2 = new Data(data);
            Data data3 = new Data();

            // Mostramos su informacion
            Console.WriteLine($"Constructor normal: {data}");
            Console.WriteLine($"Constructor con copia: {data2}");
            Console.WriteLine($"Constructor vacio: {data3}\n\n");


            // Mostramos la fecha original
            Console.WriteLine($"0 dias: {data}");

            // Sumamos 10 dias a data por lo que tiene que dar como resultado 20/12/2020
            Data masDiez = data.Sumar(10);
            Console.WriteLine($"+10 dias: {masDiez}");

            // Sumamos -10 dias a data por lo que tiene que dar como resultado 30/11/2020
            Console.WriteLine($"-10 dias: {data.Sumar(-10)}");

            // Monstramos la diferencia de una fecha recien creada. Resultado: 5 dias
            Data data4 = new Data(05, 12, 2000);
            Console.WriteLine($"Hay una separacion de {data2.Separacion(data4)} dias");

            // Creamos las fechas que vamos a usar para comparar
            Data dataComparar1 = new Data(10, 10, 2020);
            Data dataComparar2 = new Data(10, 10, 2020);

            // Resultado: Iguales
            Console.WriteLine("\n\n" + dataComparar1.Comparar(dataComparar2));

            // Cambiamos el mes de data2 a 11 para que el resultado sea Anterior
            dataComparar2.month = 11;
            Console.WriteLine(dataComparar1.Comparar(dataComparar2));

            // Cambiamos el mes de data1 a 12 para que el resultado sea Posterior
            dataComparar1.month = 12;
            Console.WriteLine(dataComparar1.Comparar(dataComparar2));
        }
    }
}
