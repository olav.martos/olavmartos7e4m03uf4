﻿using System;
using System.Drawing;

/*
 * # Protected
 * + Public
 * - Privated 
 */

namespace OlavMartos7e4M03UF4
{
    // Clase FiguraGeometrica
    public class FiguraGeometrica
    {
        // Atributos
        protected int codi;

        public int GetCodi()
        {
            return codi;
        }
        public void SetCodi(int value)
        {
            codi = value;
        }

        protected string nom;

        public string GetNom()
        {
            return nom;
        }

        public void SetNom(string value)
        {
            nom = value;
        }

        protected Color color;
        public Color GetColor()
        {
            return color;
        }
        public void SetColor(Color value)
        {
            color = value;
        }
        
        // Constructores
        public FiguraGeometrica() { }
        public FiguraGeometrica(int codi, string nom, Color color)
        {
            this.codi = codi;
            SetNom(nom);
            this.color = color;
        }
        public FiguraGeometrica(FiguraGeometrica figura)
        {
            SetCodi(figura.GetCodi());
            SetNom(figura.GetNom());
            SetColor(figura.GetColor());
        }

        // Metodo
        public override string ToString() { return $"Codi={codi}, Nom={GetNom()}, {color}"; }
    }

    // Clase Rectangle
    public class Rectangle : FiguraGeometrica
    {
        // Atributos
        private double rectBase;
        public double GetBase()
        {
            return rectBase;
        }

        public void SetBase(double value)
        {
            rectBase = value;
        }

        private double altura;
        public double GetAltura()
        {
            return altura;
        }

        public void SetAltura(double value)
        {
            altura = value;
        }

        // Constructores
        public Rectangle() { }
        public Rectangle(int codi, string nom, Color color, double baseRect, double altura) : base(codi, nom, color)
        {
            rectBase = baseRect;
            this.altura = altura;
        }
        public Rectangle(Rectangle rect)
        {
            SetCodi(rect.GetCodi());
            SetNom(rect.GetNom());
            SetColor(rect.GetColor());
            SetBase(rect.GetBase());
            SetAltura(rect.GetAltura());
        }

        // Metodos
        private double Perimetre() { return 2 * (rectBase + altura); }

        private double Area() { return rectBase * altura; }

        public override string ToString() { return $"{base.ToString()}, Base={rectBase}, Altura={altura}, Perimetre={Perimetre()}, Area={Area()}"; }
    }

    // Clase Triangle
    public class Triangle : FiguraGeometrica
    {
        // Atributos
        private double triBase;

        public double GetBase()
        {
            return triBase;
        }

        public void SetBase(double value)
        {
            triBase = value;
        }

        private double altura;
        public double GetAltura()
        {
            return altura;
        }
        public void SetAltura(double value)
        {
            altura = value;
        }

        // Constructores
        public Triangle() { }

        public Triangle(int codi, string nom, Color color, double baseTri, double altura) : base(codi, nom, color)
        {
            triBase = baseTri;
            this.altura = altura;
        }

        public Triangle(Triangle tri)
        {
            SetCodi(tri.GetCodi());
            SetNom(tri.GetNom());
            SetColor(tri.GetColor());
            SetBase(tri.GetBase());
            SetAltura(tri.GetAltura());
        }

        // Metodos
        private double Area() { return (triBase * altura) / 2; }

        public override string ToString() { return $"{base.ToString()}, Base={triBase}, Altura={altura}, Area={Area()}"; }
    }

    // Clase Cercle
    public class Cercle : FiguraGeometrica
    {
        // Atributo
        private double radi;

        public double GetRadi()
        {
            return radi;
        }

        public void SetRadi(double value)
        {
            radi = value;
        }

        // Constructores
        public Cercle() { }
        public Cercle(int codi, string nom, Color color, double radi) : base(codi, nom, color)
        {
            this.radi = radi;
        }
        public Cercle(Cercle Cer)
        {
            SetCodi(Cer.GetCodi());
            SetNom(Cer.GetNom());
            SetColor(Cer.GetColor());
            SetRadi(Cer.GetRadi());
        }

        // Metodos
        private double Perimetre() { return Math.Round(2 * Math.PI * radi, 2); }

        private double Area() { return Math.Round(Math.PI * Math.Pow(radi, 2), 2); }

        public override string ToString() { return $"{base.ToString()}, Radi={radi}, Perimetre={Perimetre()}, Area={Area()}"; }

        
    }

    class ProvaFigures
    {
        public static void Main(string[] args)
        {
            //PrimerConstructor();
            SegundoConstructor();
            //TercerConstructor();
        }

        public static void PrimerConstructor()
        {
            // Creacion de una instancia de Rectangle, Triangle, Cercle y FiguraGeometrica
            FiguraGeometrica fg = new FiguraGeometrica();
            Rectangle rectangle = new Rectangle();
            Triangle triangle = new Triangle();
            Cercle cercle = new Cercle();

            // Lo mostramos por pantalla el contenido de ToString();
            Console.WriteLine("FG Vacio: " + fg);
            Console.WriteLine("Rectangle Vacio: " + rectangle);
            Console.WriteLine("Triangle Vacio: " + triangle);
            Console.WriteLine("Cercle Vacio: " + cercle);
        }

        public static void SegundoConstructor()
        {
            // Creacion de una instancia de Rectangle, Triangle, Cercle y FiguraGeometrica
            FiguraGeometrica fg = new FiguraGeometrica(1, "FG1", Color.Fuchsia);
            Rectangle rectangle = new Rectangle(2, "R1", Color.Red, 10.0, 5.0);
            Triangle triangle = new Triangle(3, "T1", Color.Blue, 8.0, 6.0);
            Cercle cercle = new Cercle(4, "C1", Color.Green, 5.2);

            // Lo mostramos por pantalla el contenido de ToString();
            Console.WriteLine("FG Normal: " + fg);
            Console.WriteLine("Rectangle Normal: " + rectangle);
            Console.WriteLine("Triangle Normal: " + triangle);
            Console.WriteLine("Cercle Normal: " + cercle);
        }

        public static void TercerConstructor()
        {
            // Creacion de una instancia de Rectangle, Triangle, Cercle y FiguraGeometrica
            FiguraGeometrica fg = new FiguraGeometrica(1, "FG1", Color.Fuchsia);
            Rectangle rectangle = new Rectangle(2, "R1", Color.Red, 10.0, 5.0);
            Triangle triangle = new Triangle(3, "T1", Color.Blue, 8.0, 6.0);
            Cercle cercle = new Cercle(4, "C1", Color.Green, 5.2);

            // Creamos unas copias de estas instancias:
            FiguraGeometrica fg2 = new FiguraGeometrica(fg);
            Rectangle rectangle2 = new Rectangle(rectangle);
            Triangle triangle2 = new Triangle(triangle);
            Cercle cercle2 = new Cercle(cercle);

            // Lo mostramos por pantalla el contenido de ToString();
            Console.WriteLine("FG Copia: " + fg2);
            Console.WriteLine("Rectangle Copia: " + rectangle2);
            Console.WriteLine("Triangle Copia: " + triangle2);
            Console.WriteLine("Cercle Copia: " + cercle2);
        }
    }

}
