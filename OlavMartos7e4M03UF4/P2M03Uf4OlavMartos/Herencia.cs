﻿using System;
using System.Drawing;

namespace P2M03Uf4OlavMartos
{
    // Clase FiguraGeometrica
    public abstract class FiguraGeometrica
    {
        // Atributos con sus Getters y Setters
        protected int codi;

        public int GetCodi() { return codi; }
        public void SetCodi(int value) { codi = value; }

        protected string nom;

        public string GetNom() { return nom; }

        public void SetNom(string value) { nom = value; }

        protected Color color;
        public Color GetColor() { return color; }
        public void SetColor(Color value)  { color = value; }

        // Tres constructores: uno vacio, otro al que se le pasan datos y otro que es una copia de otra instancia
        public FiguraGeometrica() { }
        public FiguraGeometrica(int codi, string nom, Color color)
        {
            this.codi = codi;
            SetNom(nom);
            this.color = color;
        }
        public FiguraGeometrica(FiguraGeometrica figura)
        {
            SetCodi(figura.GetCodi());
            SetNom(figura.GetNom());
            SetColor(figura.GetColor());
        }

        // Metodo abstracto
        public abstract double CalcularArea();

        // Metodo
        public override string ToString() { return $"Codi={codi}, Nom={GetNom()}, {color}"; }

        // Metodos sobreescritos
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType()) return false;
            FiguraGeometrica figura = (FiguraGeometrica)obj;
            return codi == figura.codi;
        }

        public override int GetHashCode() { return codi.GetHashCode(); }

    }

    // Clase Rectangle
    public class Rectangle : FiguraGeometrica
    {
        // Atributos
        private double rectBase;
        public double GetBase() { return rectBase; }
        public void SetBase(double value) { rectBase = value; }


        private double altura;
        public double GetAltura() { return altura; }
        public void SetAltura(double value)
        {  altura = value; }

        // Tres constructores: uno vacio, otro al que se le pasan datos y otro que es una copia de otra instancia
        public Rectangle() { }
        public Rectangle(int codi, string nom, Color color, double baseRect, double altura) : base(codi, nom, color)
        {
            rectBase = baseRect;
            this.altura = altura;
        }
        public Rectangle(Rectangle rect)
        {
            SetCodi(rect.GetCodi());
            SetNom(rect.GetNom());
            SetColor(rect.GetColor());
            SetBase(rect.GetBase());
            SetAltura(rect.GetAltura());
        }

        // Metodo abstracto
        public override double CalcularArea() { return rectBase * altura; }

        // Metodos
        private double Perimetre() { return 2 * (rectBase + altura); }
        public override string ToString() { return $"{base.ToString()}, Base={rectBase}, Altura={altura}, Perimetre={Perimetre()}, Area={CalcularArea()}"; }

        // Metodos sobreescritos
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType()) return false;

            Rectangle other = (Rectangle)obj;
            return GetCodi() == other.GetCodi();
        }

        public override int GetHashCode() { return GetCodi().GetHashCode(); }
    }

    // Clase Triangle
    public class Triangle : FiguraGeometrica
    {
        // Atributos
        private double triBase;
        public double GetBase() { return triBase; }
        public void SetBase(double value) { triBase = value; }


        private double altura;
        public double GetAltura() { return altura; }
        public void SetAltura(double value) { altura = value; }

        // Tres constructores: uno vacio, otro al que se le pasan datos y otro que es una copia de otra instancia
        public Triangle() { }

        public Triangle(int codi, string nom, Color color, double baseTri, double altura) : base(codi, nom, color)
        {
            triBase = baseTri;
            this.altura = altura;
        }

        public Triangle(Triangle tri)
        {
            SetCodi(tri.GetCodi());
            SetNom(tri.GetNom());
            SetColor(tri.GetColor());
            SetBase(tri.GetBase());
            SetAltura(tri.GetAltura());
        }

        // Metodo abstracto
        public override double CalcularArea() { return (triBase * altura) / 2; }

        // Metodo
        public override string ToString() { return $"{base.ToString()}, Base={triBase}, Altura={altura}, Area={CalcularArea()}"; }

        // Metodos sobreescritos
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType()) return false;

            Triangle other = (Triangle)obj;
            return GetCodi() == other.GetCodi();
        }

        public override int GetHashCode() { return GetCodi().GetHashCode(); }

    }

    // Clase Cercle
    public class Cercle : FiguraGeometrica
    {
        // Atributo
        private double radi;
        public double GetRadi() { return radi; }
        public void SetRadi(double value) { radi = value; }

        // Tres constructores: uno vacio, otro al que se le pasan datos y otro que es una copia de otra instancia
        public Cercle() { }
        public Cercle(int codi, string nom, Color color, double radi) : base(codi, nom, color) { this.radi = radi; }
        public Cercle(Cercle Cer)
        {
            SetCodi(Cer.GetCodi());
            SetNom(Cer.GetNom());
            SetColor(Cer.GetColor());
            SetRadi(Cer.GetRadi());
        }

        // Metodo abstracto
        public override double CalcularArea() { return Math.Round(Math.PI * Math.Pow(radi, 2), 2); }

        // Metodos
        private double Perimetre() { return Math.Round(2 * Math.PI * radi, 2); }

        public override string ToString() { return $"{base.ToString()}, Radi={radi}, Perimetre={Perimetre()}, Area={CalcularArea()}"; }

        // Metodos sobreescritos
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType()) return false;

            Cercle other = (Cercle)obj;
            return GetCodi() == other.GetCodi();
        }

        public override int GetHashCode() { return GetCodi().GetHashCode(); }
    }

    class ProvaFigures
    {
        public static void Main(string[] args)
        {
            //PrimerConstructor();
            //SegundoConstructor();
            //TercerConstructor();
            SonIguales();
        }

        // Crea instancias de las tres figuras geometricas vacias.
        public static void PrimerConstructor()
        {
            // Creacion de una instancia de Rectangle, Triangle, Cercle y FiguraGeometrica
            Rectangle rectangle = new Rectangle();
            Triangle triangle = new Triangle();
            Cercle cercle = new Cercle();

            // Lo mostramos por pantalla el contenido de ToString();
            Console.WriteLine("Rectangle Vacio: " + rectangle);
            Console.WriteLine("Triangle Vacio: " + triangle);
            Console.WriteLine("Cercle Vacio: " + cercle);
        }

        // Crea instancias de las figuras geometricas y les da datos
        public static void SegundoConstructor()
        {
            // Creacion de una instancia de Rectangle, Triangle, Cercle y FiguraGeometrica
            Rectangle rectangle = new Rectangle(2, "R1", Color.Red, 10.0, 5.0);
            Triangle triangle = new Triangle(3, "T1", Color.Blue, 8.0, 6.0);
            Cercle cercle = new Cercle(4, "C1", Color.Green, 5.2);

            // Lo mostramos por pantalla el contenido de ToString();
            Console.WriteLine("Rectangle Normal: " + rectangle);
            Console.WriteLine("Triangle Normal: " + triangle);
            Console.WriteLine("Cercle Normal: " + cercle);
        }

        // Crea instancias de las figuras geometricas. Hace una copia
        public static void TercerConstructor()
        {
            // Creacion de una instancia de Rectangle, Triangle, Cercle y FiguraGeometrica
            Rectangle rectangle = new Rectangle(2, "R1", Color.Red, 10.0, 5.0);
            Triangle triangle = new Triangle(3, "T1", Color.Blue, 8.0, 6.0);
            Cercle cercle = new Cercle(4, "C1", Color.Green, 5.2);

            // Creamos unas copias de estas instancias:
            Rectangle rectangle2 = new Rectangle(rectangle);
            Triangle triangle2 = new Triangle(triangle);
            Cercle cercle2 = new Cercle(cercle);

            // Lo mostramos por pantalla el contenido de ToString();
            Console.WriteLine("Rectangle Copia: " + rectangle2);
            Console.WriteLine("Triangle Copia: " + triangle2);
            Console.WriteLine("Cercle Copia: " + cercle2);
        }

        public static void SonIguales()
        {
            Rectangle r1 = new Rectangle(1, "Rectangle 1", Color.Red, 5.0, 10.0);
            Rectangle r2 = new Rectangle(2, "Rectangle 2", Color.Blue, 10.0, 5.0);
            Rectangle r3 = new Rectangle(1, "Rectangle 3", Color.Green, 2.5, 6.0);

            if (r1.Equals(r2)) 
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("r1 y r2 son iguales");
            }
            else 
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("r1 y r2 no son iguales"); 
            }

            if (r1.Equals(r3))
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("r1 y r3 son iguales"); 
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("r1 y r3 no son iguales"); 
            }

            if (r1.GetHashCode() == r2.GetHashCode()) 
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("r1 y r2 son iguales según GetHashCode()"); 
            }
            else 
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("r1 y r2 no son iguales según GetHashCode()"); 
            }

            if (r1.GetHashCode() == r3.GetHashCode())
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("r1 y r3 son iguales según GetHashCode()"); 
            }
            else 
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("r1 y r3 no son iguales según GetHashCode()"); 
            }


            Tri();
            Console.ResetColor();
        }

        static void Tri()
        {
            Triangle t1 = new Triangle(1, "Triangle 1", Color.Red, 5.0, 10.0);
            Triangle t2 = new Triangle(2, "Triangle 2", Color.Red, 5.0, 10.0);
            Triangle t3 = new Triangle(1, "Triangle 3", Color.Red, 5.0, 10.0);

            if (t1.Equals(t2))
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("t1 y t2 son iguales");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("t1 y t2 no son iguales");
            }

            if (t1.Equals(t3))
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("t1 y t3 son iguales"); 
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("t1 y t3 no son iguales"); 
            }

            if (t1.GetHashCode() == t2.GetHashCode()) 
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("t1 y t2 son iguales según GetHashCode()"); 
            }
            else 
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("t1 y t2 no son iguales según GetHashCode()"); 
            }

            if (t1.GetHashCode() == t3.GetHashCode())
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("t1 y t3 son iguales según GetHashCode()"); 
            }
            else 
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("t1 y t3 no son iguales según GetHashCode()"); 
            }

            Cer();
        }

        static void Cer()
        {
            Cercle c1 = new Cercle(1, "Cercle 1", Color.Red, 5.0);
            Cercle c2 = new Cercle(1, "Cercle 2", Color.Red, 5.0);
            Cercle c3 = new Cercle(2, "Cercle 3", Color.Red, 5.0);

            if (c1.Equals(c2))
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("c1 y c2 son iguales"); 
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("c1 y c2 no son iguales"); 
            }

            if (c1.Equals(c3))
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("c1 y c3 son iguales"); 
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("c1 y c3 no son iguales"); 
            }

            if (c1.GetHashCode() == c2.GetHashCode()) 
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("c1 y c2 son iguales según GetHashCode()"); 
            }
            else 
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("c1 y c2 no son iguales según GetHashCode()"); 
            }

            if (c1.GetHashCode() == c3.GetHashCode())
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("c1 y c3 son iguales según GetHashCode()"); 
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("c1 y c3 no son iguales según GetHashCode()"); 
            }
        }
    }

}
