﻿using System;

namespace AhorrosCuentaBancaria
{
    public class Cuenta
    {
        public string titular { get; set; }
        public double cantidad { get; set; }

        public Cuenta(string titular)
        {
            this.titular = titular;
        }

        public Cuenta(string titular, double cantidad)
        {
            this.titular = titular;
            this.cantidad = cantidad;
        }

        public void Ingresar(double cantidad)
        {
            if (cantidad > 0) this.cantidad += cantidad;
        }

        public void Retirar(double cantidad)
        {
            this.cantidad -= cantidad;
            if (this.cantidad < 0) this.cantidad = 0;
        }

        public string toString()
        {
            return $"Esta cuenta pertenece a {titular} y tiene {cantidad} euros";
        }
    }
    class AhorrosCuentaBancaria
    {
        static void Main(string[] args)
        {
            Cuenta cuenta1 = new Cuenta("Angeles");
            Cuenta cuenta2 = new Cuenta("Fernando", 300);

            // Mostramos la informacion de las cuentas
            Console.WriteLine(cuenta1.toString());
            Console.WriteLine(cuenta2.toString());

            //Ingresa dinero en las cuentas
            cuenta1.Ingresar(300);
            cuenta2.Ingresar(400);

            // Mostramos la informacion de las cuentas
            Console.WriteLine(cuenta1.toString());
            Console.WriteLine(cuenta2.toString());

            //Retiramos dinero en las cuentas
            cuenta1.Retirar(500);
            cuenta2.Retirar(100);

            // Mostramos la informacion de las cuentas
            Console.WriteLine(cuenta1.toString());
            Console.WriteLine(cuenta2.toString());

        }
    }
}
