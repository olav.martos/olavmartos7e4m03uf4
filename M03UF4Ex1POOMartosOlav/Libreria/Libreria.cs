﻿using System;

namespace Libreria
{
    class Libro
    {
        // Atributos
        private string ISBN;

        public string GetISBN()
        {
            return ISBN;
        }

        public void SetISBN(string value)
        {
            ISBN = value;
        }



        private string Titulo;

        public string GetTitulo()
        {
            return Titulo;
        }

        public void SetTitulo(string value)
        {
            Titulo = value;
        }

        private string autor;

        public string GetAutor()
        {
            return autor;
        }

        public void SetAutor(string value)
        {
            autor = value;
        }

        private int numeroPaginas;

        public int GetNumeroPaginas()
        {
            return numeroPaginas;
        }

        public void SetNumeroPaginas(int value)
        {
            numeroPaginas = value;
        }

        // Constructores
        public Libro()
        {
        }

        public Libro(string iSBN, string titulo, string autor, int numeroPaginas)
        {
            ISBN = iSBN;
            Titulo = titulo;
            SetAutor(autor);
            SetNumeroPaginas(numeroPaginas);
        }

        // Metodos
        public string toString()
        {
            return $"El libro: {Titulo}, con ISBN: {ISBN}, creado por {GetAutor()}, tiene {GetNumeroPaginas()} paginas";
        }
    }
    class Libreria
    {
        static void Main(string[] args)
        {
            Libro libro1 = new Libro("123443232234", "Las flipantes aventuras de Miguel y su gato satanico", "Jerimias Jojo Jotason", 340);
            Libro libro2 = new Libro("5445454454564", "Las aventuras de un mosquito", "Mosquito Jr.", 200);

            Console.WriteLine(libro1.toString());
            Console.WriteLine(libro2.toString());

            Libro masPaginas = new Libro();
            if (libro1.GetNumeroPaginas() > libro2.GetNumeroPaginas())
            {
                masPaginas.SetISBN(libro1.GetISBN());
                masPaginas.SetTitulo(libro1.GetTitulo());
                masPaginas.SetAutor(libro1.GetAutor());
                masPaginas.SetNumeroPaginas(libro1.GetNumeroPaginas());
            }
            else
            {
                masPaginas = libro2;
            }

            Console.WriteLine($"{masPaginas.GetTitulo()} de {masPaginas.GetAutor()} tiene más paginas");
        }
    }
}
