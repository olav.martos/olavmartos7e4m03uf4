﻿using System;
using System.Collections.Generic;

namespace RondaRobada
{
    public class Carta
    {
        public int Numero { get; set; }
        public string Palo { get; set; }

        public Carta(int numero, string palo)
        {
            Numero = numero;
            Palo = palo;
        }

        public override string ToString()
        {
            string nombreNumero = "";
            switch (Numero)
            {
                case 1:
                    nombreNumero = "As";
                    break;
                case 10:
                    nombreNumero = "Sota";
                    break;
                case 11:
                    nombreNumero = "Caballo";
                    break;
                case 12:
                    nombreNumero = "Rey";
                    break;
                default:
                    nombreNumero = Numero.ToString();
                    break;
            }
            return $"{nombreNumero} de {Palo}";
        }
    }

    public class Baraja
    {
        private List<Carta> cartas;
        private List<Carta> cartasMonton;

        public Baraja()
        {
            cartas = new List<Carta>();
            cartasMonton = new List<Carta>();
            string[] palos = { "Espadas", "Bastos", "Oros", "Copas" };
            for (int i = 1; i < 13; i++)
            {
                if (i != 8 && i != 9)
                {
                    foreach (string palo in palos)
                    {
                        cartas.Add(new Carta(i, palo));
                    }
                }
            }
        }

        public void Barajar()
        {
            Random rnd = new Random();
            for (int i = cartas.Count - 1; i > 0; i--)
            {
                int j = rnd.Next(i + 1);
                Carta temp = cartas[i];
                cartas[i] = cartas[j];
                cartas[j] = temp;
            }
        }

        public Carta SiguienteCarta()
        {
            if (cartas.Count > 0)
            {
                Carta carta = cartas[0];
                cartasMonton.Add(carta);
                cartas.RemoveAt(0);
                return carta;
            }
            else
            {
                Console.WriteLine("\nNo hay mas cartas en la naraja");
                return null;
            }
        }

        public int CartasDisponibles()
        {
            return cartas.Count;
        }

        public List<Carta> DarCartas(int cant)
        {
            List<Carta> cartasADevolver = new List<Carta>();
            if (cartas.Count >= cant)
            {
                for (int i = 0; i < cant; i++)
                {
                    Carta carta = SiguienteCarta();
                    cartasADevolver.Add(carta);
                }
            }
            else
            {
                Console.WriteLine($"No se pueden dar cartas. Solo quedan {cartas.Count}");
            }
            return cartasADevolver;
        }

        public void CartasMonton()
        {
            if (cartasMonton.Count == 0)
            {
                Console.WriteLine("No ha salida ninguna carta aun");
            }
            else
            {
                Console.WriteLine("Han salido: ");
                foreach (Carta carta in cartasMonton)
                {
                    Console.WriteLine(carta.ToString() + "\t");
                }
            }
        }

        public void MostrarBaraja()
        {
            Console.WriteLine("Baraja: ");
            foreach (Carta carta in cartas)
            {
                Console.WriteLine(carta.ToString());
            }
        }
    }

    class RondaRobada
    {
        static void Main(string[] args)
        {
            // Creamos una Baraja
            Baraja baraja = new Baraja();

            // Barajamos
            baraja.Barajar();

            // Damos 5 cartas
            List<Carta> cartasDadas = baraja.DarCartas(5);
            foreach (Carta carta in cartasDadas)
            {
                Console.WriteLine(carta.ToString());
            }

            // Mostramos las que ya han salido
            Console.WriteLine("\n\n");
            baraja.CartasMonton();

            //// Decimos cuantas cartas quedan
            Console.WriteLine($"\nQuedan {baraja.CartasDisponibles()} cartas en la baraja.");

            // Intentamos dar mas de las que tenemos
            List<Carta> cartasDadas2 = baraja.DarCartas(50);
            foreach (Carta carta in cartasDadas2)
            {
                if (carta != null)
                {
                    Console.WriteLine("Cartas dadas:");
                    Console.WriteLine(carta.ToString());
                }
            }

            // Mostramos las cartas restantes
            List<Carta> cartasRestantes = baraja.DarCartas(baraja.CartasDisponibles());
            Console.WriteLine("\n\nCartas restantes:");
            foreach (Carta carta in cartasRestantes)
            {
                Console.WriteLine(carta.ToString());
            }
            Console.WriteLine($"\n\nQuedan {baraja.CartasDisponibles()} cartas en la baraja.");

            // Intentamos saber si existe una siguiente carta o no
            Carta cartaExtra = baraja.SiguienteCarta();

            if (cartaExtra == null) Console.WriteLine("\nNo hay siguiente carta");
            else Console.WriteLine(cartaExtra.ToString());
        }
    }
}
