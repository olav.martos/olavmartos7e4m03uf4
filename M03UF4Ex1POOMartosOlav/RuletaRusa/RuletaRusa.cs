﻿using System;
using System.Collections.Generic;

namespace RuletaRusa
{
    public class Revolver
    {
        // Atributos
        Random rnd = new Random();
        public int posActual;
        public int posBala;

        // Constructor
        public Revolver()
        {
            posActual = rnd.Next(1, 7);
            posBala = rnd.Next(1, 7);
        }
        
        // Metodos
        public bool Disparar()
        {
            if(posBala == posActual) { return true; }
            else
            {
                SiguienteBala();
                return false;
            }
        }

        public void SiguienteBala()
        {
            posActual++;
            if (posActual > 6) { posActual = 1; }
        }

        public override string ToString()
        {
            return $"La posicion del tambor es {posActual}, mientras que la bala se encuentra en la {posBala}";
        }

    }

    public class Jugador
    {
        // Atributos
        public int ID;
        public string nombre;
        public bool vivo;

        // Constructor
        public Jugador(int id)
        {
            ID = id;
            nombre = "Jugador " + id;
            vivo = true;
        }

        // Metodos
        public void Disparar(Revolver r)
        {
            Console.WriteLine($"{nombre} coloca el revolver en su sien");
            bool dead = r.Disparar();

            if (dead)
            {
                Console.WriteLine($"Lastima! {nombre} ha muerto.");
                vivo = false;
            }
            else { Console.WriteLine($"{nombre} continua con la partida. De momento..."); }
        }

        public bool IsAlive() { return vivo; }
    }

    public class Juego
    {
        // Atributos
        public List<Jugador> jugadores;
        public Revolver revolver;
        public Random rnd;
        public bool finJuego;

        // Constructor
        public Juego(int num)
        {
            if (num < 1 || num > 6) { num = 6; }

            jugadores = new List<Jugador>();
            revolver = new Revolver();
            for (int i = 1; i <= num; i++) { jugadores.Add(new Jugador(i)); }

            revolver = new Revolver();
            rnd = new Random();
            finJuego = false;
        }

        // Metodos
        public bool FinJuego() { return true; }

        public void Ronda()
        {
            foreach (Jugador jugador in jugadores)
            {
                if (jugador.IsAlive())
                {
                    Console.WriteLine(revolver.ToString());
                    Console.WriteLine(jugador.nombre + " se prepara para disparar...");
                    jugador.Disparar(revolver);
                    Console.WriteLine("Estado de la partida:");
                    foreach (Jugador player in jugadores)
                    {
                        Console.Write($"{player.nombre}: ");

                        if (player.IsAlive()) VivoMuerto(ConsoleColor.Green, "Vivo") ; 
                        else VivoMuerto(ConsoleColor.Red, "Muerto");
                    }
                    Console.WriteLine("-------------");

                    if (!jugador.IsAlive())
                    {
                        finJuego = true;
                        Console.WriteLine(jugador.nombre + " ha muerto, el juego ha terminado.");
                        break;
                    }
                }
            }
        }

        public void VivoMuerto(ConsoleColor color, string status)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(status);
            Console.ResetColor();
        }
    }

    class RuletaRusa
    {
        static void Main()
        {
            Juego juego = new Juego(6);
            juego.Ronda();
            juego.FinJuego();
        }
    }
}
