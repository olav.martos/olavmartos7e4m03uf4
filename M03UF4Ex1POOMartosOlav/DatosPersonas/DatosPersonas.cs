﻿using System;

namespace DatosPersonas
{
    class Persona
    {
        // Atributos
        public string nombre = " ";
        public void Setnombre(string value)
        {
            nombre = value;
        }

        public string apellidos = " ";
        public void Setapellidos(string value)
        {
            apellidos = value;
        }

        public int edad = 0;
        public void Setedad(int value)
        {
            edad = value;
        }

        public string DNI { get; set; }

        public char sexo = 'H';
        public void Setsexo(char value)
        {
            sexo = comprobarSexo(value);
        }

        public double peso = 0.0;
        public void Setpeso(double value)
        {
            peso = value;
        }

        public double altura = 0.0;
        public void Setaltura(double value)
        {
            altura = value;
        }

        // Constructores
        public Persona()
        {
            nombre = "Pepito";
            apellidos = "Martinez Martinez";
            edad = 16;
            DNI = generaDNI();
            sexo = 'H';
            peso = 77.5;
            altura = 1.76;
        }

        public Persona(string nombre, string apellidos, int edad, char sexo)
        {
            this.nombre = nombre;
            this.apellidos = apellidos;
            this.edad = edad;
            this.sexo = comprobarSexo(sexo);
            DNI = generaDNI();
            peso = 76.5;
            altura = 1.76;
        }

        public Persona(string nombre, string apellidos, int edad, char sexo, double peso, double altura)
        {
            this.nombre = nombre;
            this.apellidos = apellidos;
            this.edad = edad;
            this.DNI = generaDNI();
            this.sexo = comprobarSexo(sexo);
            this.peso = peso;
            this.altura = altura;
        }

        // Metodos
        public int CalcularIMC()
        {
            double IMC = peso / Math.Pow(altura, 2);
            if (IMC < 20) return -1;
            if (IMC >= 20 && IMC <= 25) return 0;
            if (IMC > 25) return 1;
            return 0;
        }
        public bool esMayorDeEdad()
        {
            if (edad >= 18) return true;
            else return false;
        }

        char comprobarSexo(char sexo)
        {
            if(sexo == 'H' || sexo == 'M')
            {
                return sexo;
            }
            return 'H';
        }

        public override string ToString()
        {
            return $"{nombre} {apellidos}, edad {edad}, sexo {sexo}. DNI: {DNI}. Tiene un peso de {peso} con una altura de {altura}";
        }

        string generaDNI()
        {
            Random rnd = new Random();
            char letra = (char)(('A') + rnd.Next(26));
            string dni = Convert.ToString(rnd.Next(10000000, 100000000)) + Convert.ToString(letra);
            return dni;
        }

    }
    class DatosPersonas
    {
        static void Main(string[] args)
        {
            Console.Write("Nombre de una persona: ");
            string nombre = Console.ReadLine();

            Console.Write("Apellidos de una persona: ");
            string apellidos = Console.ReadLine();

            Console.Write("Edad de una persona: ");
            int edad = Convert.ToInt32(Console.ReadLine());

            Console.Write("Sexo de esa persona: ");
            char sexo = Convert.ToChar(Console.ReadLine());

            Console.Write("Peso de la persona: ");
            double peso = Convert.ToDouble(Console.ReadLine());

            Console.Write("Altura de la persona: ");
            double altura = Convert.ToDouble(Console.ReadLine());

            // Los tres objetos
            Persona p1 = new Persona(nombre, apellidos, edad, sexo, peso, altura);
            Persona p2 = new Persona(nombre, apellidos, edad, sexo);
            Persona p3 = new Persona();
            p3.Setnombre(nombre);
            p3.Setapellidos(apellidos);
            p3.Setedad(edad-4);
            p3.Setsexo(sexo);
            p3.Setpeso(120.0);
            p3.Setaltura(1.85);

            // Comprovamos su peso ideal
            ComprobarPeso(p1.nombre, p1.CalcularIMC());
            ComprobarPeso(p2.nombre, p2.CalcularIMC());
            ComprobarPeso(p3.nombre, p3.CalcularIMC());

            // Indicamos si es mayor de edad
            MayorEdad(p1.nombre, p1.esMayorDeEdad());
            MayorEdad(p2.nombre, p2.esMayorDeEdad());
            MayorEdad(p3.nombre, p3.esMayorDeEdad());

            // Mostramos la informacion de cada objeto
            Console.WriteLine(p1.ToString());
            Console.WriteLine(p2.ToString());
            Console.WriteLine(p3.ToString());

        }

        static void ComprobarPeso(string nombre, int IMC)
        {
            if (IMC == -1) 
            { 
                Console.WriteLine($"{nombre} esta en su peso ideal"); 
                return; 
            }
            if (IMC == 0)
            {
                Console.WriteLine($"{nombre} esta por debajo de su peso ideal");
                return;
            }
            if(IMC == 1)
            {
                Console.WriteLine($"{nombre} tiene sobrepeso");
                return;
            }

        }

        static void MayorEdad(string nombre, bool mayor)
        {
            if (mayor) { Console.WriteLine($"{nombre} es mayor de edad"); }
            else { Console.WriteLine($"{nombre} es menor de edad"); }
        }
    }
}
