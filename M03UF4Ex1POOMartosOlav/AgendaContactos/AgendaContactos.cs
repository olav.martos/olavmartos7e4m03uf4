﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AgendaContactos
{

    class Contacto
    {
        // Atributos
        public string nombre { get; set; }
        public int telefono { get; set; }

        // Constructor
        public Contacto(string nombre, int telefono)
        {
            this.nombre = nombre;
            this.telefono = telefono;
        }

        // Metodos
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            Contacto otroContacto = (Contacto)obj;
            return nombre == otroContacto.nombre;
        }

        public override int GetHashCode()
        {
            return nombre.GetHashCode();
        }
    }

    class Agenda
    {
        // Atributo
        List<Contacto> contactos { get; set; }
        private int capacidad { get; set; }

        // Constructores
        public Agenda(int tamanyo)
        {
            capacidad = tamanyo;
            contactos = new List<Contacto>(tamanyo);
        }

        public Agenda()
        {
            capacidad = 10;
            contactos = new List<Contacto>(10);
        }

        // Metodos

        public void AnyadirContacto(Contacto c)
        {
            if (contactos.Any(cont => cont.nombre == c.nombre))
            {
                Console.WriteLine("Ya existe un contacto con el mismo nombre");
                return;
            }

            if (contactos.Count == contactos.Capacity)
            {
                Console.WriteLine("No se pueden añadir más contactos a la agenda");
                return;
            }

            contactos.Add(c);
        }

        public bool ExisteContacto(Contacto c)
        {
            foreach (Contacto cont in contactos)
            {
                if (cont.nombre == c.nombre && cont.telefono == c.telefono)
                {
                    return true;
                }
            }
            return false;
        }

        public void ListarContactos()
        {
            if(contactos.Count == 0)
            {
                Console.WriteLine("No hay contenido que listar");
                return;
            }
            foreach (Contacto c in contactos)
            {
                Console.WriteLine($"{c.nombre} - {c.telefono}");
            }
        }

        public void BuscaContacto(string nombre)
        {
            foreach (Contacto c in contactos)
            {
                if (c.nombre == nombre)
                {
                    Console.WriteLine($"Telefono de {c.nombre}: {c.telefono}");
                    return;
                }
            }
            Console.WriteLine("No se ha encontrado ningun telefono relacionado con ese nombre");
        }

        public void EliminarContacto(Contacto c)
        {
            int index = contactos.FindIndex(cont => cont.nombre == c.nombre);

            if (index == -1)
            {
                Console.WriteLine($"El contacto: {c.nombre} - {c.telefono} esta fuera de servicio por y para siempre");
                return;
            }

            contactos.RemoveAt(index);
            Console.WriteLine($"El contacto: {c.nombre} - {c.telefono} ha sido eliminado de la faz de la existencia terrenal");
        }

        public bool AgendaLlena()
        {
            return contactos.Count >= capacidad;
        }

        public int HuecosLibres()
        {
            return capacidad - contactos.Count;
        }
    }

    class AgendaContactos
    {
        static void Main(string[] args)
        {
            Agenda agenda = new Agenda();
            int option;

            do
            {
                Console.WriteLine("0.- Salir");
                Console.WriteLine("1.- Añadir contacto");
                Console.WriteLine("2.- Existe este contacto?");
                Console.WriteLine("3.- Listar contactos");
                Console.WriteLine("4.- Buscar contacto");
                Console.WriteLine("5.- Eliminar un contacto");
                Console.WriteLine("6.- La agenda esta llena?");
                Console.WriteLine("7.- Cuantos huecos libres quedan");
                Console.Write("> ");
                option = Convert.ToInt32(Console.ReadLine());

                Menu(option, agenda);
                
            } while (option != 0);
            Console.WriteLine("Goodbye");
        }

        static void Menu(int option, Agenda agenda)
        {
            string nombre = "";
            int tel = 0;
            switch (option)
            {
                case 1:
                    Option1(nombre, tel, agenda);
                    break;
                case 2:
                    Option2(nombre, tel, agenda);
                    break;
                case 3:
                    agenda.ListarContactos();
                    break;
                case 4:
                    Option4(nombre, agenda);
                    break;
                case 5:
                    Option5(nombre, tel, agenda);
                    break;
                case 6:
                    Option6(agenda);
                    break;
                case 7:
                    Console.WriteLine($"Quedan {agenda.HuecosLibres()} huecos libres");
                    break;
            }
            Console.WriteLine("Pulse cualquier tecla para continuar");
            Console.ReadLine();
            Console.Clear();
        }

        static void Option1(string nombre, int tel, Agenda agenda)
        {
            Console.Write("Nombre del contacto: ");
            nombre = Console.ReadLine();
            Console.Write("Telefono del contacto: ");
            tel = Convert.ToInt32(Console.ReadLine());
            Contacto contacto = new Contacto(nombre, tel);

            agenda.AnyadirContacto(contacto);
        }

        static void Option2(string nombre, int tel, Agenda agenda)
        {
            Console.Write("Nombre del contacto a comprobar: ");
            nombre = Console.ReadLine();
            Console.Write("Telefono del contacto a comprobar: ");
            tel = Convert.ToInt32(Console.ReadLine());
            Contacto contacto = new Contacto(nombre, tel);

            if (agenda.ExisteContacto(contacto)) Console.WriteLine("Existe");
            else Console.WriteLine("No existe");
        }

        static void Option4(string nombre, Agenda agenda)
        {
            Console.Write("Nombre del contacto: ");
            nombre = Console.ReadLine();
            agenda.BuscaContacto(nombre);
        }

        static void Option5(string nombre, int tel, Agenda agenda)
        {
            Console.Write("Nombre del contacto a comprobar: ");
            nombre = Console.ReadLine();
            Console.Write("Telefono del contacto a comprobar: ");
            tel = Convert.ToInt32(Console.ReadLine());

            Contacto contacto = new Contacto(nombre, tel);

            if (agenda.ExisteContacto(contacto)) agenda.EliminarContacto(contacto);
            else Console.WriteLine("No se puede eliminar algo que no existe");
        }

        static void Option6(Agenda agenda)
        {
            if (agenda.AgendaLlena()) Console.WriteLine("Si esta llena");
            else  Console.WriteLine("No esta llena");
        }
    }
}
